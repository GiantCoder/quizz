desc 'Fetch # googleplus data'
task :fetch_googleplus => :environment do
	require 'rubygems'
	require 'nokogiri'
	require 'open-uri'

	@brands = Brand.league('Premier League')
	today = Date.today

	def get_googleplus
		@brands.each do |brand|
			url = "#{brand.google_plus}"
			doc = Nokogiri::HTML(open(url))

			# Getting Google Plus data
			begin
				if doc.at_css('span[1].BOfSxb').text != nil
					followers = doc.at_css('span[1].BOfSxb').text.delete(',').to_i
					followers_metrics = Metric.create(google_plus_followers: followers)
					brand.metrics << followers_metrics
				end
			rescue
				puts "Oops! Something went wrong accessing Google Plus followers for #{brand.name}"
				next
			end

			begin
				if doc.at_css('span[3].BOfSxb').text != nil
					views = doc.at_css('span[3].BOfSxb').text.delete(',').to_i
					view_metrics = Metric.create(google_plus_views: views)
					brand.metrics << view_metrics
				end
			rescue
				puts "Oops! Something went wrong accessing Google Plus views for #{brand.name}"
				next
			end

			puts "----------------------------------------------"
			puts "#{brand.name}: #{followers} followers"
			puts "#{brand.name}: #{views} views"
			puts "----------------------------------------------"

			sleep rand(1..3)
		end
		puts "ALL DONE! :)"
	end

get_googleplus

end