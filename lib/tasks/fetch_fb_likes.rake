desc 'Fetch # Facebook Likes'
time = Time.now

task :fetch_fb_likes => :environment do
	require 'rubygems'
	require 'HTTParty'

	@brands = Brand.Premiership
	today = Date.today
	access_token = "CAACEdEose0cBAOCnPAnUaVZAPM1FX7mZA8RnIZBPt5c4ddjtGyt0LZCoiygDDo32F0ueADmCGjZAmucrSvSeEClCuzZCvA03k2AimeKbRZCst4Bl3nD3a9Ld6rZCUTxBDMcc3fC71f4OjnPzn79TH8ZAYoOQiOr3eOXLdDGgJ07Ie1xyZCojRGZALqaDrjkY2NZBeWlaoh3f0LtBPJy3S7Gu3ZCYN"
	#Facebook data to capture
	fields = "likes,talking_about_count"

	@brands.each do |brand|
		rand = rand(1..5)
		sleep rand
		if rand == 1
			puts "Sorry, almost nodded off...."
		else
			puts "Slept for #{rand} seconds.... zzzzz"
		end
		node = brand.fb_node
		url = "https://graph.Facebook.com/#{node}?fields=#{fields}&access_token=#{access_token}"
		puts "---------------------------"	
		puts "Okay, fetching Facebook Likes for #{brand.name}"
		doc = HTTParty.get(url)
		puts "Opening URL"
		fbdata = doc.parsed_response
		likes = fbdata.to_a[0][1]
		puts "---------------------------"
		puts "#{brand.name} has reached a cumulative total of #{likes} Facebook Likes today!"
		@metric = Metric.create(fb_likes: likes)
		puts "---------------------------"
		puts "Adding data to database...."
		brand.metrics << @metric
	end

	time_elapsed = Time.now - time

	puts "Total Job Execution time: #{time_elapsed}"
end