desc 'Fetch # Twitter Tweets'
task :fetch_twitter => :environment do
	require 'rubygems'
	require 'nokogiri'
	require 'open-uri'

	@brands = Brand.sport('AUSRUL')
	today = Date.today

	def get_twitter
		@brands.each do |brand|
			url = "#{brand.twitter_url}"
			doc = Nokogiri::HTML(open(url))

			tweets_raw = []
			following_raw = []
			followers_raw = []
			favorites_raw = []

			# Getting Twitter data
			begin
				if doc.at_css('.ProfileNav-stat') != nil
					doc.at_css('.ProfileNav-stat').each do |line|
						tweets_raw << line
					end
					tweets = tweets_raw[1][1].gsub(/\s.+/,'').delete(',').to_i
					@tweet_metric = Metric.create(tweets: tweets)
					brand.metrics << @tweet_metric
				end
				if doc.at_css('.ProfileNav-item--following a') != nil
					doc.at_css('.ProfileNav-item--following a').each do |line|
						following_raw << line
					end
					following = following_raw[1][1].delete(',').to_i
					@following_metric = Metric.create(following: following)
					brand.metrics << @following_metric
				end
				if doc.at_css('.ProfileNav-item--followers a') != nil
					doc.at_css('.ProfileNav-item--followers a').each do |line|
						followers_raw << line
					end
					followers = followers_raw[1][1].to_s.gsub(/\s.+/,'').delete(',').to_i
					followers_metric = Metric.create(followers: followers)
					brand.metrics << followers_metric
				end
				if doc.at_css('.ProfileNav-item--favorites a') != nil
					doc.at_css('.ProfileNav-item--favorites a').each do |line|
						favorites_raw << line
					end
					favorites = favorites_raw[1][1].delete(',').to_i
					favorites_metric = Metric.create(favorites: favorites)
					brand.metrics << favorites_metric
				end
			rescue
				puts "Oops! Something went wrong accessing 'tweets' for #{brand.name}"
				next
			end

			puts "----------------------------------------------"
			puts "#{brand.name}: #{tweets} tweets"
			puts "#{brand.name}: following #{following} accounts"
			puts "#{brand.name}: #{followers} followers"
			puts "#{brand.name}: #{favorites} favorites"
			puts "----------------------------------------------"

			sleep rand(1..3)
		end
		puts "ALL DONE! :)"
	end

get_twitter

end