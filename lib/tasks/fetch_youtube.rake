desc 'Fetch # Youtube data'
task :fetch_youtube => :environment do
	require 'rubygems'
	require 'nokogiri'
	require 'open-uri'

	@brands = Brand.gender('F')
	today = Date.today

	def get_youtube
		@brands.each do |brand|
			url = "#{brand.youtube_url}/about"
			doc = Nokogiri::HTML(open(url))

			subscribers = 0
			views = 0

			# Getting Youtube data
			begin
				if doc.at_css('span.about-stat b').text != nil
					subscribers = doc.at_css('span.about-stat b').text.delete(',').to_i
					views = doc.at_css('span[2].about-stat b').text.delete(',').to_i
					joined = doc.at_css('span[4].about-stat').text.to_s[6..16]
					if joined[0] = " "
						joined[0] = "0"
					end
				subscriber_metrics = Metric.create(yt_subs: subscribers)
				brand.metrics << subscriber_metrics
				view_metrics = Metric.create(yt_views: views)
				brand.metrics << view_metrics
				joined = DateTime.parse(joined).to_date.to_s
				brand.update(yt_joined: joined)
				end
			rescue
				puts "Oops! Something went wrong accessing Youtube data for #{brand.name}"
				next
			end

			puts "----------------------------------------------"
			puts "#{brand.name}: #{subscribers} subscribers"
			puts "#{brand.name}: #{views} views"
			puts "#{brand.name}: joined Youtube on #{joined}"
			puts "----------------------------------------------"

			sleep rand(1..3)
		end
		puts "ALL DONE! :)"
	end

get_youtube

end