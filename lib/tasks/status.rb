require "net/http"

def status()
	file = File.open("websites.txt").read
	file.gsub!(/\r\n?/, "\n")
	results = File.open("results.csv", "w")
	line = 0

	file.each_line do |site|
		url = URI.parse(site)
		req = Net::HTTP.new(url.host, url.port)
		req.use_ssl = (url.scheme == "https")
			begin
				res = req.request_head(url.path)			
			rescue
				results << "Something went wrong accessing #{site}"
				next
			end
		string = url += "," + res.code
		results << string
		results << "\n"
		line += 1
		puts "#{line}: #{url.host} - #{res.code}"
		if line % 10 == 0
			sleep rand(1.3)
	end
	end
	results.close()
end

status()
