desc 'Fetch # Majestic data'
time = Time.now

task :fetch_majestic => :environment do
	require 'HTTParty'

	@brands = Brand.Championship
	today = Date.today
	apikey = "C5AF3A274EEF2BF25F25138FA822D941"

	@brands.each do |brand|
		url = "http://api.majestic.com/api/json?app_api_key=#{apikey}&cmd=GetIndexItemInfo&items=1&item0=#{brand.website}&datasource=fresh"
		doc = HTTParty.get(url)
		data = doc.parsed_response.to_a

		links = data[11][1]["Results"]["Data"][0]["ExtBackLinks"]
		puts "#{brand.name} has reached a cumulative total of #{links} links."
		urds = data[11][1]["Results"]["Data"][0]["RefDomains"]
		puts "#{brand.name} has reached a cumulative total of #{urds} referring domains."
		tf = data[11][1]["Results"]["Data"][0]["TrustFlow"]
		puts "#{brand.name} has a TrustFlow score of #{tf}."
		cf = data[11][1]["Results"]["Data"][0]["CitationFlow"]
		puts "#{brand.name} has a TrustFlow score of #{cf}."
		topic0 = data[11][1]["Results"]["Data"][0]["TopicalTrustFlow_Topic_0"]
		puts "#{brand.name} has a topic of #{topic0}"
		topic1 = data[11][1]["Results"]["Data"][0]["TopicalTrustFlow_Topic_1"]
		puts "#{brand.name} has a topic of #{topic1} "
		topic2 = data[11][1]["Results"]["Data"][0]["TopicalTrustFlow_Topic_2"]
		puts "#{brand.name} has a topic of #{topic2}"
		puts "--------------------------------------"
		@metric = Metric.new
		@metric.update(freshlinks: links)
		@metric.update(urds: urds)
		@metric.update(trustflow: tf)
		@metric.update(citationflow: cf)
		@metric.update(topic0: topic0)
		@metric.update(topic1: topic1)
		@metric.update(topic2: topic2)

		@metric.save

		brand.metrics << @metric
	end

	time_elapsed = Time.now - time

	puts "-----------------------------------"
	puts "Total Job Execution time: #{time_elapsed}"
end