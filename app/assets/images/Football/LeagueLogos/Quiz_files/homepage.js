(function() {
  jQuery(function() {
    return Morris.Line({
      element: 'questions_chart',
      data: $('#questions_chart').data('questions'),
      xkey: 'created_at',
      ykeys: ['diff'],
      labels: ['Difficulty']
    });
  });

}).call(this);
