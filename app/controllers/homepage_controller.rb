class HomepageController < ApplicationController

	def index
		if params[:tag]
    		@questions = Question.tagged_with(params[:tag])
  		else
    		@questions = Question.all
		end
		
		@quizzs = Quizz.all
		@users = User.all
		@brands = Brand.all
	end

end
