class CommentsController < ApplicationController
 before_filter :load_commentable

  def index
  	@commentable = Article.find(params[:article_id])
  	@comments = commentable.comments

  end
 
  def new
  end

  private

  def load_commentable
  	resource, id = request.path.split('/')[1, 2]
  	@commentable = resource.singularize.classify.constantize.find(id)
  end
end
