module QuestionsHelper

	def questions_chart_data
		(12.weeks.ago.to_date..Date.today).map do |date|
			{
				created_at: date,
				diff: Question.where("date(created_at) = ?", date).count
			}
		end
	end
end
