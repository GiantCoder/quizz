class BrandPdf < Prawn::Document
	def initialize(brand)
		super(top_margin: 30, left_margin: 30)
		@brand = brand
		brand_name
		brand_details
	end

	def brand_name
		text "Brand: #{@brand.name}", size: 30, style: :bold
	end

	def brand_details
		move_down 20
		text "Brand details: \n"

		text "the cursor is here: #{cursor}"
		text "now it is here: #{cursor}"

		move_down 200
		text "on the first move the cursor went down to #{cursor}"

		move_up 100
		text "on the second move the cursor went up to #{cursor}"

		move_cursor_to 50
		text "on the third move the cursor went directly to #{cursor}"

		stroke_axis
		stroke_circle [0, 0], 10
		bounding_box([100, 300], :width => 300, :height => 200) do
			stroke_bounds
			stroke_circle [0, 0], 20
		end

		stroke_horizontal_rule
		pad(20) { text "Text padded both before and after" }

		stroke_horizontal_rule
		pad_top(20) { text "Text padded on the top." }

		stroke_horizontal_rule
		pad_bottom(20) { text "Text padded on the bottom." }

		stroke_horizontal_rule
		move_down 30

		text "Text written before the float block."

		float do
			move_down 30
			bounding_box([0, cursor], :width => 200) do
				text "Text written inside the float block."
				stroke_bounds
			end
		end

		text "Text written after the float block."

		start_new_page
		text "I'm on the next page!"

		require "prawn/measurement_extensions"

		[:mm, :cm, :dm, :m, :in, :yd, :ft].each do |measurement|
			text "1#{measurement} in PDF Points: #{1.send(measurement)} pt"
			move_down 5.mm
		end

		start_new_page
		stroke_axis
		stroke_axis(:at => [70,70], :height => 200, :step_length => 30,
			:negative_axes_length => 15, :color => '0000FF')
		stroke_axis(:at => [140, 140], :width => 200, :height => cursor.to_i - 140,
			:step_length => 20, :negative_axes_length => 40, :color => 'FF00')

		start_new_page
		stroke_axis
		stroke_color 'ff0000'

		# No block
		line [0, 200], [100, 150]

		fill_color '0000FF'
		rectangle [10, 100], 100, -220
		fill

		# With block
		stroke { line [200, 200], [300, 150] }
		fill { rectangle [ 200, 100 ], 100, 10 }

		# Method hook
		stroke_line [400, 200], [500, 150]
		fill_rectangle [400, 100], 100, 100

		start_new_page
		stroke_axis

		# Triangle
		stroke_polygon [50, 200], [50, 300], [150, 300]

		# Hexagon
		fill_polygon [50, 150], [150, 200], [250, 150],
					 [250, 50], [150, 0], [50, 50]

		# Pentagram
		pentagon_points = [500, 100], [430, 5], [319, 41], [319, 159], [430, 195]
		pentagram_points = [0, 2, 4, 1, 3].map { |i| pentagon_points[i] }

		stroke_rounded_polygon(20, *pentagram_points)

		start_new_page

		stroke_axis

		stroke_circle [100, 300], 100

		fill_ellipse [200, 100], 100, 50

		fill_ellipse [400, 100], 50

	end
end