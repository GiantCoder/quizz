class Question < ActiveRecord::Base
#	before_action :authenticate_member!, :except => [:index, :show]
	has_many :quizzs, through: :quizz_questions
	has_many :quizz_questions
	belongs_to :user
	acts_as_taggable
	
	scope :approved, -> { where(is_approved: true ) }
	scope :today, lambda { where('DATE(created_at) = ?', Date.today) }
	scope :recent, lambda { where('DATE(created_at) < ?', 10.days.ago) }

end