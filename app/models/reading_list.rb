class ReadingList < ActiveRecord::Base
  belongs_to :article
  belongs_to :user
  has_many :comments, as: :commentable
end
