class Brand < ActiveRecord::Base
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "30x30>" }, default_url: "/images/:style/missing.png"
  validates_attachment :logo, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :bg_image, styles: { vlarge: "2800x1600", large: "800x600", medium: "300x300>" }, default_url: "/images/:style/missing.png"
  validates_attachment :bg_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  belongs_to :user
  belongs_to :category
  has_many :metrics, dependent: :destroy

  scope :Premiership, -> { where('league = ?', 'Premier League' ) }
  scope :MLS, -> { where('league = ?', 'MLS' ) }
  scope :Championship, -> { where('league = ?', 'Championship' ) }
  scope :L1, -> { where('league = ?', 'League One' ) }
  scope :L2, -> { where('league = ?', 'League Two' ) }
  scope :Rugby, -> { where('sport = ?', 'Rugby' ) }
  scope :NFL, -> { where('sport = ?', 'NFL' ) }
  scope :Basketball, -> { where('sport = ?', 'Basketball' ) }
  scope :Baseball, -> { where('sport = ?', 'Baseball' ) }
  scope :Hockey, -> { where('sport = ?', 'ICEHOCK' ) }
  scope :RugbyLeague, -> { where('sport = ?', 'RUGLG' ) }
  scope :female, -> { where('gender = ?', 'F' ) }
  scope :today, -> { where('DATE(created_at) = ?', Date.today) }
  scope :recent, -> { where('DATE(created_at) < ?', 10.days.ago) }

  scope :country, -> (country) { where country: country }
  scope :gender, -> (gender) { where gender: gender }
  scope :league, -> (league) { where league: league }
  scope :sport, -> (sport) { where sport: sport }
  scope :website, -> (website) { where website: nil }

  scope :node, -> { where("fb_node is NOT NULL and fb_node != ''") }
  scope :node, -> { where("website is NULL") }
 
	def self.to_csv(options = {})
		CSV.generate(headers: true) do |csv|
			csv << column_names
			all.each do |brand|
				csv << brand.attributes.values_at(*column_names)
			end
		end
	end

	def self.to_csv2
	attributes = %w{id name}
	
		CSV.generate(headers: true) do |csv|
			csv.add_row column_names
			all.each do |foo|
				values = foo.attributes.values
				csv.add_row values
				csv << "\n"
			end
		end
	end

	def self.import(file)
		CSV.foreach(file.path, headers: true) do |row|
			Brand.create! row.to_hash
		end
	end
end
