class User < ActiveRecord::Base
	has_many :questions
	has_many :quizzs
	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "50x50>" }, default_url: "/images/:style/missing.png"
	validates_attachment :avatar, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

	def self.total_on(date)
		where("date(created_at) = ?", date).count
	end

	def age
		return (Date.today - self.created_at.to_date).to_i
	end

end
