class Quizz < ActiveRecord::Base
	has_many :quizz_questions
	has_many :questions, through: :quizz_questions
	belongs_to :user
	has_many :comments, as: :commentable
	acts_as_taggable
end
