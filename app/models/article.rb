class Article < ActiveRecord::Base
  belongs_to :user
  has_many :articlemetrics
  has_many :comments, as: :commentable
  acts_as_taggable

end
