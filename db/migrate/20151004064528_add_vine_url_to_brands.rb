class AddVineUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :vine_url, :string
  end
end
