class AddLatLongToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :lat, :float
    add_column :brands, :long, :float
  end
end
