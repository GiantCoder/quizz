class AddNicknameToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :nickname, :string
  end
end
