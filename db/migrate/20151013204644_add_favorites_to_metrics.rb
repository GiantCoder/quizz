class AddFavoritesToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :favorites, :integer
  end
end
