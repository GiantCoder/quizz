class AddTripadvisorUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :tripadvisor_url, :string
  end
end
