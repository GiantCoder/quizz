class AddGooglePlusFollowersToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :google_plus_followers, :integer
  end
end
