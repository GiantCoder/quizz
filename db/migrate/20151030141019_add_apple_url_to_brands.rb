class AddAppleUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :apple_url, :string
  end
end
