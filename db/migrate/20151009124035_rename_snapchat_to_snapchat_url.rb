class RenameSnapchatToSnapchatUrl < ActiveRecord::Migration
  def change
  	rename_column :brands, :snapchat, :snapchat_url
  end
end
