class AddAudioboomUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :audioboom_url, :string
  end
end
