class AddGenderToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :gender, :string, :limit => 1
  end
end
