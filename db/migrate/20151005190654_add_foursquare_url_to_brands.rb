class AddFoursquareUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :foursquare_url, :string
  end
end
