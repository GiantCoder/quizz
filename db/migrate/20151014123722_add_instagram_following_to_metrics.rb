class AddInstagramFollowingToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :instagram_following, :integer
  end
end
