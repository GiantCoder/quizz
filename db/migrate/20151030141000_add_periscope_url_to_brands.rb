class AddPeriscopeUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :periscope_url, :string
  end
end
