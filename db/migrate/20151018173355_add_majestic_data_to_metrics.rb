class AddMajesticDataToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :freshlinks, :integer
    add_column :metrics, :urds, :integer
    add_column :metrics, :trustflow, :integer, :limit => 3
    add_column :metrics, :citationflow, :integer, :limit => 3
    add_column :metrics, :topic0, :string
    add_column :metrics, :topic1, :string
    add_column :metrics, :topic2, :string
  end
end
