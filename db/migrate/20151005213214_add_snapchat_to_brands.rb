class AddSnapchatToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :snapchat, :string
  end
end
