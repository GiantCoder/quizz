class AddBgImageToBrands < ActiveRecord::Migration
  def self.up
      add_attachment :brands, :bg_image
  end

  def self.down
    remove_attachment :brands, :bg_image
  end
end
