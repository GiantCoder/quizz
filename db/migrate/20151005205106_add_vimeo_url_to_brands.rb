class AddVimeoUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :vimeo_url, :string
  end
end
