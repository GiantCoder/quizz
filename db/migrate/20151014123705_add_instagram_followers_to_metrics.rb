class AddInstagramFollowersToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :instagram_followers, :integer
  end
end
