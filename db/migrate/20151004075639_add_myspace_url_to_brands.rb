class AddMyspaceUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :myspace_url, :string
  end
end
