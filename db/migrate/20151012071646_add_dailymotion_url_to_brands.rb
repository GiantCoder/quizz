class AddDailymotionUrlToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :dailymotion_url, :string
  end
end
