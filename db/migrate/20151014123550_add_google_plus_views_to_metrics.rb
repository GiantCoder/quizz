class AddGooglePlusViewsToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :google_plus_views, :integer
  end
end
