class AddInstagramPostsToMetrics < ActiveRecord::Migration
  def change
    add_column :metrics, :instagram_posts, :integer
  end
end
