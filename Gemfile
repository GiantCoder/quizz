source 'https://rubygems.org'

# Use mysql as the database for Active Record
gem 'mysql2'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.6'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use devise for authentication
gem 'devise', :git => "https://github.com/plataformatec/devise.git"
gem "therubyracer"
gem "less-rails"
gem "twitter-bootstrap-rails"
#For handling image uploads
gem 'paperclip', '~> 4.3.1'
#For downloading PDFs
gem 'prawn', '~> 2.0.2'
#For handing Prawn tables
gem 'prawn-table', '~> 0.2.2'
#Need for chartkick
gem 'groupdate', '~> 2.5.0'
#Highcharts - also need for Chartkick
gem 'highcharts-rails', '~> 4.1.8'
#For one line charting
gem 'chartkick', '~> 1.4.1'
#For scraping JS-based sites, e.g. Instagram
gem 'phantomjs', '~> 1.9.8.0'


#Capybara is an integration testing tool for rack based web applications.
#It simulates how a user would interact with a website
gem 'capybara', '~> 2.5.0'
#Poltergeist is a driver for Capybara that allows you to run your tests on a headless WebKit browser, provided by PhantomJS.
gem 'poltergeist', '~> 1.7.0'

gem 'activeadmin',         github: 'gregbell/active_admin'
gem 'inherited_resources', github: 'josevalim/inherited_resources'
gem 'simple_form', '~> 3.2.0'
gem 'formtastic',          github: 'justinfrench/formtastic'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
#Tags
gem 'acts-as-taggable-on', '~> 3.5.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Could also use gem 'active_model_serializers', '~> 0.9.3'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc
#For consuming external APIs
gem 'httparty', '~> 0.13.7'
#For OAuth
gem 'omniauth', '~> 1.2.2'
gem 'omniauth-oauth2', '~> 1.3.1'
gem 'omniauth-facebook', '~> 2.0.1'
gem 'omniauth-twitter', '~> 1.2.1'

#For Facebook Graph data
gem 'koala', '~> 2.2.0'
#For crawling
gem 'mechanize', '~> 2.7.3'
#For scraping
gem 'nokogiri', '~> 1.6.6.2'
#For Cron jobs
gem 'whenever', '~> 0.9.4'

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
# Better errors: https://github.com/charliesome/better_errors
group :development, :test do

  gem "better_errors"
  gem 'binding_of_caller'
  gem 'spring'
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

